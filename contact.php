<?php 
    include('config.php');
    $obj = new db_class();
    if(isset($_POST['submit'])){
        $name = trim($_POST['name']); 
        $email = trim($_POST['email']);
        $mobile_no = trim($_POST['mobile_no']);
        $category_id = trim($_POST['category_id']);        
        $today = date("Y-m-d H:i:s"); 
        $msg = $obj->addMsg($name,$email,$mobile_no,$category_id,$today);
    }
?>
<!DOCTYPE HTML>

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <title>Cloth Bag Vending Maching</title>
   <!-- CSS -->
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
   <link href="css/animate.css" rel="stylesheet">
   <link href="css/bootsnav.css" rel="stylesheet">
   <link href="css/owl.carousel.min.css" rel="stylesheet">
   <link href="css/owl.theme.default.min.css" rel="stylesheet">
   <link href="css/style.css" rel="stylesheet">
   <link rel="icon" type="image/png" href="images/favicon.png">
   <link href="aos/aos.css" rel="stylesheet">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
   <script>
      new WOW().init();
   </script>
   <style>
      /* .ctms_mf {
         padding-top: 14px;
         padding-bottom: 18px;
         padding-left: 20px;
         font-size: 32px;
         padding-right: 18px;
         font-weight: 700;
         font-family: SF Pro Display, SF Pro Icons, Helvetica Neue, Helvetica, Arial, sans-serif;
         color: #6e6e73;
         letter-spacing: .004em;
         line-height: 1.3;
      } */

      .ctms_mf {
         padding-top: 14px;
         padding-bottom: 18px;
         padding-left: 20px;
         font-size: 32px;
         padding-right: 18px;
         font-weight: 700;
         font-family: SF Pro Display, SF Pro Icons, Helvetica Neue, Helvetica, Arial, sans-serif;
         letter-spacing: .004em;
         line-height: 1.3;
         background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
         -webkit-background-clip: text;
         -webkit-text-fill-color: transparent;
         font-size: 31px;
         font-weight: 600;
      }

      @media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) {
         h2.CTMS_heading {
            text-align: center;
            font-size: 42px;
            padding-top: 5px;
         }

         .col-sm-4.first {
            text-align: center;
            padding-top: 5em;
         }

         .col-sm-6.secound {
            padding-top: 7em;
         }

         .col-sm-6.first {
            text-align: center;
            padding-top: 7em;
         }

         h2.fdsfdsfdsf {
            padding-top: 0em;
         }

         .col-sm-4.firsts {
            text-align: center;
            padding-top: 6em;
         }

         p.text-justify.CTMS_mon_headerrrrsd {
            font-size: 20px;
            text-align: center;
         }

         div#monitoring {
            padding-top: 5em;
         }

         .foot-nav {
            text-align: center;
         }

         .col-xl-4.col-md-4.mb-30 {
            text-align: center;
         }

         .copy-right {
            text-align: center;
         }

         .navbar-right {
            float: right !important;
            margin-right: -29px;
         }

         .ctms_mf {
            padding-top: 14px;
            padding-bottom: 18px;
            padding-left: 20px;
            font-size: 24px;
            padding-right: 18px;
            font-weight: 700;
            font-family: SF Pro Display, SF Pro Icons, Helvetica Neue, Helvetica, Arial, sans-serif;
            color: #6e6e73;
            letter-spacing: .004em;
            line-height: 1.3;
         }

         .col-md-6.frt {
            margin-top: 4em;
         }

         section#about_contrive {
            padding-top: 1em;
         }

         .CTMS_iot_image {
            font-size: 9px;
            font-weight: 500;
            margin-bottom: 12px;
            line-height: 1.3;
            padding-top: 3em;
            text-align: center;
         }

         div#monitoyyyring {
            padding-top: 6em;
         }


      }

      @media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : landscape) {
         h2.CTMS_heading {
            text-align: center;
            font-size: 42px;
            padding-top: 0em;
         }

         .col-sm-4.first {
            text-align: center;
            padding-top: 16em;
         }

         .col-sm-6.secound {
            padding-top: 7em;
         }

         .col-sm-6.first {
            text-align: center;
            padding-top: 7em;
         }

         h2.fdsfdsfdsf {
            padding-top: 1em;
         }

         .col-sm-4.firsts {
            text-align: center;
            padding-top: 6em;
         }

         p.text-justify.CTMS_mon_headerrrrsd {
            font-size: 17px;
         }

         div#monitoring {
            padding-top: 5em;
         }

         .foot-nav {
            text-align: center;
         }

         .col-xl-4.col-md-4.mb-30 {
            text-align: center;
         }

         .copy-right {
            text-align: center;
         }

         .navbar-right {
            float: right !important;
            margin-right: -29px;
         }

         .ctms_mf {
            padding-top: 14px;
            padding-bottom: 18px;
            padding-left: 20px;
            font-size: 27px;
            padding-right: 18px;
            font-weight: 700;
            font-family: SF Pro Display, SF Pro Icons, Helvetica Neue, Helvetica, Arial, sans-serif;
            color: #6e6e73;
            letter-spacing: .004em;
            line-height: 1.3;
         }

         .CTMS_mon_headerrrr {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 25px;
            font-weight: 600;
            padding-left: 2em;
         }

         div#monitoyyyring {
            padding-top: 10em;
         }

         img.real_time_img {
            width: 18em;
         }

         iframe.vide {
            width: 100%;
            height: 45em
         }
      }

      @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {

         .hu {
            display: none;
         }

         .ctms_mf {
            padding-top: 14px;
            padding-bottom: 18px;
            padding-left: 20px;
            font-size: 17px;
            padding-right: 18px;
            font-weight: 700;
            font-family: SF Pro Display, SF Pro Icons, Helvetica Neue, Helvetica, Arial, sans-serif;
            color: #6e6e73;
            letter-spacing: .004em;
            line-height: 1.3;
         }

         .col-md-6.frt {
            margin-top: 2em;
         }

         .fgft {
            background-image: linear-gradient(90deg, #2e1f7c, #6652e1);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-family: SF Pro Display, SF Pro Icons, Helvetica Neue, Helvetica, Arial, sans-serif;
            font-weight: bold;
            font-size: 17px;
         }

         section.iuy {
            padding-top: 1em;
         }

         #monitoring {
            padding-top: 0em;
         }

         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 28px;
            font-weight: 600;
         }

         .CTMS_iot_image {
            font-size: 6px;
         }

         .col-md-4.res{
            padding-bottom: 2em;
         }

         img.ddd{
            width: 21em;
            padding-left: 3em;
         }

         .CTMS_mon_headerr {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 24px;
            font-weight: 600;
         }

         h2.qr_co {
            font-size: 18px;
            font-weight: 700;
            margin-bottom: -16px;
            line-height: 1.3;
            padding-top: 5%;
            text-align: center;
            background-image: linear-gradient(90deg, #2e1f7c, #6652e1);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-family: SF Pro Display, SF Pro Icons, Helvetica Neue, Helvetica, Arial, sans-serif;
         }

         .kiuyt {
            text-align: center;
            font-size: 19px;
            background-image: linear-gradient(90deg, #2e1f7c, #6652e1);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-family: SF Pro Display, SF Pro Icons, Helvetica Neue, Helvetica, Arial, sans-serif;
         }

         .CTMS_mon_headerrrr {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 18px;
            font-weight: 600;
            padding-left: 2em;
         }

         .fdsfdsfdsf {
            text-align: center;
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
         }

         section#about_contrive {
            padding-top: 2em;
         }

         .CTMS_mon_headerrrrsd {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 17px;
            font-weight: 600;
            text-align: center;
         }

         div#monitoyyyring {
            padding-top: 2em;
         }

         img.logo_ctms {
            padding-top: 4em;
            height: 18em;
         }

         .col-md-6.headerw {
            padding-top: 0em;
         }

         img.logo_ctms_secound {
            width: 22em;
            height: 24em;
         }

         iframe.vide {
            width: 100%;
            height: 45em
         }

      }



      @media only screen and (max-device-width : 1200px) {
         .huu {
            display: none;
         }
      }

      section.about-hp {
         text-align: center;
         padding: 40px 0 8px 0;
         margin-top: 0px;
      }

      .qr_code_text {
         padding: 7px;
         background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
         -webkit-background-clip: text;
         -webkit-text-fill-color: transparent;
         font-weight: 700;
      }

      .kiuyt {
         text-align: center;
         font-size: 19px;
         background-image: linear-gradient(90deg, #2e1f7c, #6652e1);
         -webkit-background-clip: text;
         -webkit-text-fill-color: transparent;
         font-family: SF Pro Display, SF Pro Icons, Helvetica Neue, Helvetica, Arial, sans-serif;
      }

      section#about_contrive {
         padding-top: 3em;
      }

      .fgtry {
         background-image: linear-gradient(90deg, #2e1f7c, #6652e1);
         -webkit-background-clip: text;
         -webkit-text-fill-color: transparent;
         font-weight: 600;
         font-size: 24px;
      }

      section.about_contrive_rt {
         padding-top: 3em;
      }

      span.dsds {
         padding-left: 80px;
      }

      div#monitoyyyring {
         padding-top: 10em;
      }

      img.real_time_img {
         width: 33em;
      }

      @media only screen and (max-width: 768px) {
         .col-sm-6.first {
            text-align: center;
            padding-top: 2em;
         }

         img.IoT_img {
            width: 36em;
         }

         .monitoring_img {
            width: 29em;
         }

         .col-sm-4 {
            padding-bottom: 9px;
         }

         .desktop_code {
            display: none;
         }

         .ResponsiveCode {
            display: block;
         }

         iframe.vide {
            width: 100%;
            height: 45em
         }

      }

      /* ----------- iPad 1, 2, Mini and Air ----------- */

      /* Portrait and Landscape */
      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (-webkit-min-device-pixel-ratio: 1) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         .CTMS_mon_headerr {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 22px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 20em;
         }

         img.IoT_img {
            width: 18em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }
      }

      /* Portrait */
      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 1) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         .CTMS_mon_headerr {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 22px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 20em;
         }

         img.IoT_img {
            width: 18em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }

      }

      /* Landscape */
      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 1) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         .CTMS_mon_headerr {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 22px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 20em;
         }

         img.IoT_img {
            width: 18em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }

         .col-md-6.headerw {
            padding-top: 0em;
         }

      }

      /* ----------- iPad 3, 4 and Pro 9.7" ----------- */

      /* Portrait and Landscape */
      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (-webkit-min-device-pixel-ratio: 2) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         .CTMS_mon_headerr {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 22px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 20em;
         }

         img.IoT_img {
            width: 18em;
         }

         img.img-fluid.ddf {
            width: 50em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }

      }

      /* Portrait */
      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         .CTMS_mon_headerr {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 22px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 34em;
         }

         img.IoT_img {
            width: 18em;
         }

         img.img-fluid.ddf {
            width: 50em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }
      }

      /* Landscape */
      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         .CTMS_mon_headerr {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 22px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 20em;
         }

         img.IoT_img {
            width: 18em;
         }

         img.img-fluid.ddf {
            width: 50em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }
      }

      /* ----------- iPad Pro 10.5" ----------- */

      /* Portrait and Landscape */
      @media only screen and (min-device-width: 834px) and (max-device-width: 1112px) and (-webkit-min-device-pixel-ratio: 2) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         .CTMS_mon_headerr {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 22px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 20em;
         }

         img.IoT_img {
            width: 18em;
         }

         img.img-fluid.ddf {
            width: 50em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }

         .title_message {
            display: none;
         }
      }

      /* Portrait */
      /* Declare the same value for min- and max-width to avoid colliding with desktops */
      /* Source: https://medium.com/connect-the-dots/css-media-queries-for-ipad-pro-8cad10e17106*/
      @media only screen and (min-device-width: 834px) and (max-device-width: 834px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         .CTMS_mon_headerr {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 22px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 20em;
         }

         img.IoT_img {
            width: 18em;
         }

         img.img-fluid.ddf {
            width: 50em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }
      }

      /* Landscape */
      /* Declare the same value for min- and max-width to avoid colliding with desktops */
      /* Source: https://medium.com/connect-the-dots/css-media-queries-for-ipad-pro-8cad10e17106*/
      @media only screen and (min-device-width: 1112px) and (max-device-width: 1112px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 20em;
         }

         img.IoT_img {
            width: 18em;
         }

         img.img-fluid.ddf {
            width: 50em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }
      }

      /* ----------- iPad Pro 12.9" ----------- */

      /* Portrait and Landscape */
      @media only screen and (min-device-width: 1024px) and (max-device-width: 1366px) and (-webkit-min-device-pixel-ratio: 2) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 20em;
         }

         img.IoT_img {
            width: 18em;
         }

         img.img-fluid.ddf {
            width: 50em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }

      }

      /* Portrait */
      /* Declare the same value for min- and max-width to avoid colliding with desktops */
      /* Source: https://medium.com/connect-the-dots/css-media-queries-for-ipad-pro-8cad10e17106*/
      @media only screen and (min-device-width: 1024px) and (max-device-width: 1024px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 20em;
         }

         img.IoT_img {
            width: 18em;
         }

         img.img-fluid.ddf {
            width: 50em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }
      }

      /* Landscape */
      /* Declare the same value for min- and max-width to avoid colliding with desktops */
      /* Source: https://medium.com/connect-the-dots/css-media-queries-for-ipad-pro-8cad10e17106*/
      @media only screen and (min-device-width: 1366px) and (max-device-width: 1366px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2) {
         .CTMS_mon_header {
            background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-size: 23px;
            font-weight: 600;
         }

         img.real_time_img {
            width: 20em;
         }

         img.IoT_img {
            width: 18em;
         }

         img.img-fluid.ddf {
            width: 50em;
         }

         .col-md-12.lft-abt-hp.wow.fadeInUp.water_motor_automation {
            text-align: center;
         }

         iframe.ctms_video {
            width: 100%;
            height: 35em;
            padding-left: 8em;
            padding-right: 8em
         }

         .monitoring_img {
            width: 29em;
            padding-left: 14em;
         }

         h5.card-title.Dashboard {
            font-size: 14px;
         }

         h5.card-title.instant {
            font-size: 16px;
         }

         h5.card-title.ipad_co {
            padding-top: 25px;
         }

         h5.card-title.real_time {
            font-size: 13px;
         }

         h5.card-title.citizen {
            font-size: 21px;
         }

         section.about_contrive_rt {
            padding-top: 0.3em;
         }

      }

      iframe.ctms_video {
         width: 100%;
         height: 35em;
      }

      .col-sm-6.four {
         text-align: center;
         padding-top: 4em;
      }

      /* .monitoring_img {
         width: 46em;
      } */

      .card-body {
         -ms-flex: 1 1 auto;
         flex: 1 1 auto;
         padding: 1.25rem;
      }

      .card {
         position: relative;
         display: -ms-flexbox;
         display: flex;
         -ms-flex-direction: column;
         flex-direction: column;
         min-width: 0;
         word-wrap: break-word;
         background-color: #fff;
         background-clip: border-box;
         border: 1px solid rgba(0, 0, 0, .125);
         border-radius: 1.25rem;
      }


      .card.mb-3 {
         background: #f5f5f7;
         padding: 1em;
      }

      .card.mb-4.overload {
         background: #f5f5f7;
         padding: 8em;
      }

      h5.card-title {
         text-align: center;
         background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
         -webkit-background-clip: text;
         -webkit-text-fill-color: transparent;
         text-transform: uppercase;
      }

      img.card-img-top {
         height: 5em;
         /* width: 8em; */
      }

      img.card-img-cont {
         height: 11em;
         width: 11em;
      }

      .dinn1 {
         text-align: center;
      }

      h5.card-title.real_time {
         font-size: 14px;
      }

      /* .ResponsiveCode {
         display: none;
      } */

      td {
         text-align: center;
         font-size: 1.3em;
         -webkit-background-clip: text;
         -webkit-text-fill-color: transparent;
         font-family: SF Pro Display, SF Pro Icons, Helvetica Neue, Helvetica, Arial, sans-serif;
         font-weight: bold;
         background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
         -webkit-background-clip: text;
         -webkit-text-fill-color: transparent;
      }

      @media (min-width:1025px) {
         .ResponsiveCode {
            display: none;
         }
      }
   </style>
</head>

<body>
   <header>
      <div class="clearfix"></div>
   </header>

   <style>
      @media screen and (min-width: 1400px) {
         section.about_contrive_rt.reso_code {
            display: none;
         }

         section.about_contrive_rt.contrive {
            display: none;
         }
      }

      @media screen and (min-width: 1600px) {
         section.about_contrive_rt.reso_code {
            display: none;
         }

         section.about_contrive_rt.contrive {
            display: none;
         }
      }

      @media screen and (min-width: 1900px) {
         section.about_contrive_rt.reso_code {
            display: none;
         }

         section.about_contrive_rt.contrive {
            display: none;
         }
      }

      h2.card-title.water {
         text-align: center;
         background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
         -webkit-background-clip: text;
         -webkit-text-fill-color: transparent;
         padding-top: 1em;
      }

      .copyright {
         text-align: center;
         padding: 1em;
         background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);
         -webkit-background-clip: text;
         -webkit-text-fill-color: transparent;
         font-weight: 700;
      }
   </style>

<?php if(isset($msg)){echo $msg;} ?>
<section>
      <div class="container">
        <div style="text-align: end;padding-top: 4em;">    
   </div>
         <div class="row">
            <div class="col-sm-12">
               <div class="lft-abt-hp wow fadeInUp">
                  <div class="col-md-2" style="padding-top: 1em;">
                  </div>
                  <div class="col-md-8 lft-abt-hp wow fadeInUpBig">
                     <img class="logo_ctms" src="header2.png">
                  </div>
                  <div class="col-md-2"></div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="aboutt-hp" id="get_sales">
      <div class="container" style="background: linear-gradient(#e66465, #9198e5);padding: 3em;">
         <p class="contr"> Contact our sales team</p>
         <form method="post" class="php-email-form" data-aos="fade-up" data-aos-delay="100">
            <div class="form-row">
               <div class="form-group col-md-6">
                  <label style="color: #fff;">Name</label>
                  <input type="text" name="name" class="form-control" placeholder="Name" required>
               </div>
               <div class="form-group col-md-6">
                  <label style="color: #fff;">Email</label>
                  <input type="text" name="email" class="form-control" placeholder="email" required>
               </div>
            </div>
            <div class="form-row">
               <div class="form-group col-md-6">
                  <label style="color: #fff;">Mobile No</label>
                  <input type="text" name="mobile_no" class="form-control" placeholder="Mobile no" required>
               </div>
               <div class="form-group col-md-6">
                  <label style="color: #fff;">Choose Product</label>
                  <select class="form-control" name="category_id" required>
                     <option selected>Choose...</option>
                     <option value="1">100 Bags Machine (Rs 90,000)</option>
                     <option value="2">150 Bags Machine (Rs 100,000)</option>
                     <option value="3">500 Bags Machine (Rs 150,000)</option>
                  </select>
               </div>
            </div>
            <div class="text-center">
               <input type="submit" class="btn btn-light" name="submit" value="Submit" />
            </div>
         </form>
      </div>
   </section>
   <div style="text-align: center;padding-top: 17px;">
      <a href="Cloth_bag_vending_machine.pdf" class="dowlaod_button" target="_blank"><i class="fa fa-download"></i>
         Download Brochure
      </a>
   </div>
   <hr>
   <section class="copyright-footer">
      <div class="container">
         <div class="foot-social col-lg-6 col-md-6">
            <p
               style="margin-bottom: 1em;text-align: center;background-image: linear-gradient(90deg, #2ca2b4, #5598de 24%, #7f87ff 45%, #f65aad 76%, #ec3d43);-webkit-background-clip: text;-webkit-text-fill-color: transparent;font-weight: 700;">
               sales@contrive.consulting <br> 0755-3591423</p>
         </div>
         <div class="foot-social col-lg-6 col-md-6">
            <ul style="text-align: center;">
               <li><a href="https://www.facebook.com/people/Kapde-Ka-Thaila/100088742374342/" target="_blank"><i
                        class="fa fa-facebook" aria-hidden="true"></i> </a></li>
               </li>
               <li><a href="https://www.instagram.com/kapdekathaila/" target="_blank"><i class="fa fa-instagram"
                        aria-hidden="true"></i> </a></li>
            </ul>
         </div>
   </section>

   <div class="copyright">
      Copyright © 2022 kapdekathaila.com. All rights reserved.
   </div>


   <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   <script src="js/bootstrap.min.js"></script>
   <!-- Bootsnavs -->
   <script src="js/bootsnav.js"></script>
   <script src="js/wow.min.js"></script>
   <script src="js/owl.carousel.js"></script>
   <script src="js/common-script.js"></script>
   <script>
      var video = document.getElementById("myVideo");
      var btn = document.getElementById("myBtn");

      function myFunction() {
         if (video.paused) {
            video.play();
            btn.innerHTML = "Pause";
         } else {
            video.pause();
            btn.innerHTML = "Play";
         }
      }
   </script>

</body>

</html>